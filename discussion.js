// Create fruits document
db.fruits.insertMany([
	{
		"name": "Apple",
		"color": "Red",
		"stock": 20,
		"price": 40,
		"supplier_id": 1,
		"onSale": true,
		"origin": ["Philippines", "US"]
	},
	{
		"name": "Banana",
		"color": "Yellow",
		"stock": 15,
		"price": 20,
		"supplier_id": 2,
		"onSale": true,
		"origin": ["Philippines", "Ecuador"]
	},
	{
		"name": "Kiwi",
		"color": "Green",
		"stock": 25,
		"price": 50,
		"supplier_id": 1,
		"onSale": true,
		"origin": ["US", "China"]
	},
	{
		"name": "Mango",
		"color": "Yellow",
		"stock": 10,
		"price": 120,
		"supplier_id": 2,
		"onSale": false,
		"origin": ["Philippines", "India"]
	}
]);

// MongoDB Aggregation
// used to generate manipulated data and perform operations to create filtered results that help in analyzing data.

// Using Aggregate method
/* $match
	- used to pass documents that meet the specified conditions(s) to the next pipeline stage/aggregation proces.
	- Syntax: { $match: {field: value} }

$group
	- used to group elements together and field-value pairs using the data from the grouped elements
	- Syntax: { $group: { _id: "value", fieldResult: "valueResult" } }
*/

// Match fruits on sale only and group by supplier. Include total stocks available.
db.fruits.aggregate([
	{ $match: { "onSale": true } },
	{ $group: { "_id": "$supplier_id", "total": { $sum: "$stock" } } }
]);

// Sorting aggregated results
/* $sort
	- can be used to change the order of aggregated result
	- Syntax: { $sort { field: 1/-1 } }
*/

// Sort by lowest stock first
db.fruits.aggregate([
	{ $match: { "onSale": true } },
	{ $group: { "_id": "$supplier_id", "total": { $sum: "$stock" } } },
	{ $sort: { "total": 1 } }
]);

// Aggregateing results based on array fields
/* $unwind
	- deconstructs an array field from a collection/field with an array value to output a result for each element. Each output document is the input document with the value of the array field replaced by the element.
	- Syntax: { $unwind: field }
*/

// deconstruct the document according to country origin
db.fruits.aggregate([
	{ $unwind: "$origin" }
]);

// group based on the origin and how many kinds of fruit are supplied
db.fruits.aggregate([
	{ $unwind: "$origin" },
	{ $group: { "_id": "$origin", kinds: { $sum: 1 } } }
]);

// Schema Design

// One-to-One Relationship

var owner = ObjectId();

db.owners.insert({
	_id: owner,
	name: "John Smith",
	contact: "09876543210"
});

db.suppliers.insert({
	name: "ABC Fruits",
	contact: "09998876543",
	owner_id: ObjectId("6177705e602f8f91f7bd5c96")
});

// SAMPLE: Update the owner document and add the supplier id
db.owners.updateOne(
	{"_id": ObjectId("6177705e602f8f91f7bd5c96") },
	{
		$set: {
			"supplier_id" : ObjectId("6177716d602f8f91f7bd5c97")
		}
	}
);

/*Result:
	_id: ObjectId("6177705e602f8f91f7bd5c96"),
	_id: owner,
	name: "John Smith",
	contact: "09876543210"*/

// One-to-Many Relationship
db.suppliers.insertMany([
	{
		"name": "DEF Fruits",
		"contact": "09632109847",
		"owner_id": ObjectId("6177705e602f8f91f7bd5c96")
	},
	{
		"name": "GHI Fruits",
		"contact": "09065489712",
		"owner_id": ObjectId("6177705e602f8f91f7bd5c96")
	}
]);

db.owners.updateOne(
	{"_id": ObjectId("6177705e602f8f91f7bd5c96")},
	{
		$set: {
			"supplier_id": [ObjectId("6177716d602f8f91f7bd5c97"), ObjectId("61777a07602f8f91f7bd5c98"), ObjectId("61777a07602f8f91f7bd5c99")]
		}
	}
);
